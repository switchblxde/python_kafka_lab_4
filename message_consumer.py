from kafka import KafkaConsumer

# Create a Kafka consumer
consumer = KafkaConsumer('timestamps', bootstrap_servers=['localhost:9092'], consumer_timeout_ms=1000)

# Read and print the messages from the topic
message_count = 0
for message in consumer:
    print(message.value.decode('utf-8'))
    message_count += 1

consumer.close()
