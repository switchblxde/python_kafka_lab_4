from kafka import KafkaProducer
from datetime import datetime

# Create a Kafka producer
producer = KafkaProducer(bootstrap_servers=['localhost:9092'])

# Define the topic to which we will be sending messages
topic = 'timestamps'

# Send a thousand messages with timestamps
for i in range(1000):
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    message = f'{i}: {timestamp}'
    producer.send(topic, message.encode('utf-8'))

# Flush and close the producer to ensure all messages are sent
producer.flush()
producer.close()
