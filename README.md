# Requirements

1. `docker-compose`
2. `kafka-python` (not `kafka`, it's important !)

# How to launch application

1. Launch the bootstrap server using `docker-compose`:

    `docker-compose up -d`

2. Launch both the producer and the consumer simultaneously: 

    `python3 message_producer.py & python3 message_consumer.py `

